package com.test.android.articles;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.test.android.R;

public class ArticleDetailsFragment extends Fragment {

    private ImageView mImageView;
    private ImageView mBackButtonView;

    private TextView mHeaderTextView;
    private TextView mDescriptionTextView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_article_details, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setBackgroundResource(R.color.gray_100);

        ConstraintLayout layout = view.findViewById(R.id.constraint);

        mBackButtonView = view.findViewById(R.id.back_button);
        mImageView = view.findViewById(R.id.article_image);
        mHeaderTextView = view.findViewById(R.id.article_header);
        mDescriptionTextView = view.findViewById(R.id.article_description);

        String name = getArguments().getString("name");
        String description = getArguments().getString("description");
        String image = getArguments().getString("image");

        mHeaderTextView.setText(name);
        mDescriptionTextView.setText(description);
        if (image != null && !image.isEmpty()) {
            Glide.with(requireContext())
                    .load(image)
                    .into(mImageView);
        }

        mBackButtonView.setOnClickListener(view1 -> {
            requireActivity().getSupportFragmentManager().popBackStack();
        });

        layout.setOnClickListener(view1 -> {});
    }
}
