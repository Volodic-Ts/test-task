package com.test.android.helpers;

import android.view.View;

import com.test.android.firebase.Article;

public interface OnArticleClickListener {
    void onItemClick(Article article, int position);
}
