package com.test.android.helpers;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.test.android.R;
import com.test.android.firebase.Article;

import java.util.List;

public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.ArticlesHolder> {

    private static final int EMPTY_LIST_TYPE = 0;
    private static final int NON_EMPTY_LIST_TYPE = 1;

    private Context mContext;
    private Activity mActivity;
    private List<Article> listArticles;

    private final OnArticleClickListener onArticleClickListener;

    public ArticlesAdapter(Context mContext, Activity mActivity, List<Article> listArticles, OnArticleClickListener onArticleClickListener) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.listArticles = listArticles;
        this.onArticleClickListener = onArticleClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        return listArticles.isEmpty() ? EMPTY_LIST_TYPE : NON_EMPTY_LIST_TYPE;
    }

    @NonNull
    @Override
    public ArticlesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType == EMPTY_LIST_TYPE) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.article_no_item, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.article_item, parent, false);
        }

        return new ArticlesHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticlesHolder mainHolder, int position) {
        if (getItemViewType(position) == EMPTY_LIST_TYPE) {
            return;
        }

        ArticlesHolder holder = (ArticlesHolder) mainHolder;
        Article article = listArticles.get(position);

        String imageUrl = article.getImageUrl();
        if (imageUrl != null && !imageUrl.isEmpty()) {
            Glide.with(mContext)
                    .load(imageUrl)
                    .into(holder.image);
        }
        holder.name.setText(article.getName());
        holder.description.setText(article.getDescription());

        holder.itemView.setOnClickListener(view -> onArticleClickListener.onItemClick(article, position));
    }

    @Override
    public int getItemCount() {
        if (listArticles.size() == 0) {
            return 1;
        } else {
            return listArticles.size();
        }
    }

    public static class ArticlesHolder extends RecyclerView.ViewHolder{
        TextView name;
        TextView description;
        ImageView image;

        public ArticlesHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.card_header);
            description = itemView.findViewById(R.id.card_text);
            image = itemView.findViewById(R.id.card_image);
        }
    }
}
