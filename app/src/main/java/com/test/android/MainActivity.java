package com.test.android;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.test.android.articles.ArticleDetailsFragment;
import com.test.android.firebase.Article;
import com.test.android.helpers.ArticlesAdapter;
import com.test.android.helpers.OnArticleClickListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private DatabaseReference mDatabaseReference;

    private RecyclerView mRecycleView;

    private List<Article> articleList = new ArrayList<>();
    private ArticlesAdapter adapter = null;

    private Activity mActivity;
    private Context mContext;

    private OnArticleClickListener onArticleClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Objects.requireNonNull(getSupportActionBar()).hide();

        mActivity = MainActivity.this;
        mContext = getApplicationContext();

        FirebaseApp.initializeApp(this);

        mRecycleView = findViewById(R.id.articles_view);
        mRecycleView.setLayoutManager(new LinearLayoutManager(this));

        parseDataFromFirebaseDB();

        onArticleClickListener = (article, position) -> {

             Bundle bundle = new Bundle();
             bundle.putString("name", article.getName());
             bundle.putString("description", article.getDescription());
             bundle.putString("image", article.getImageUrl());

             ArticleDetailsFragment fragment = new ArticleDetailsFragment();
             fragment.setArguments(bundle);

             getSupportFragmentManager().beginTransaction()
                     .add(R.id.container, fragment)
                     .addToBackStack("article")
                     .commit();
         };
    }

    private void parseDataFromFirebaseDB() {
        mDatabaseReference = FirebaseDatabase.getInstance().getReference().child("articles");
        mDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                articleList.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    Article article = dataSnapshot.getValue(Article.class);
                    articleList.add(article);
                }
                adapter = new ArticlesAdapter(mContext, mActivity, articleList, onArticleClickListener);
                mRecycleView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(MainActivity.this,"Error:" + error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }



    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else {
            finish();
        }
    }
}